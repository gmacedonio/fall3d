# Example test case
The FALL3D distribution includes multiple test cases (benchmark suite) that can
be downloaded from the [central repository](https://gitlab.com/fall3d-suite).
These cases are used to check the model configuration and installation rather
than to perform an accurate simulations for event reconstruction.

Let's run the example case from the `Example` folder. First, enter the folder 
and use the `wget` command to download the input meteorological file required 
by FALL3D:
```console
> cd Example
> wget https://gitlab.com/fall3d-distribution/testsuite/-/raw/master/example-8.0/InputFiles/example-8.0.wrf.nc
```

You can execute FALL3D in serial mode using the input file `Example.inp` with the following command:
```console
> ../bin/Fall3d.x Example.inp All
```

> **Notes:**
> * Depending on your installation the executable file name `Fall3d.x`
>   can be different
> * More input files for testing can be downloaded from the
>   [Benchmark suite repository](https://gitlab.com/fall3d-distribution/testsuite)

## Checking the results
If the run was successful, you should obtain a log file `Example.Fall3d.log`
with the end message:
```
  Number of warnings : 0
  Number of errors   : 0
  Task FALL3D        : ends NORMALLY
```
The results of the simulation are stored in the output netCDF file
`Example.res.nc`. For a quick view of the output file, you can use the `ncview`
tool (available [here](http://meteora.ucsd.edu/~pierce/ncview_home_page.html))
to graphically display netCDF files:
```console
> ncview Example.res.nc
```
