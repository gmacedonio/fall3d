# Running FALL3D using Docker

Another method to install and run Fall3D is by using Docker containers. 
In the official [Fall3D repository](https://gitlab.com/fall3d-suite), you can 
find a section called "Container Registry," which contains the two available
images of Fall3D. These images are available for Linux AMD64 and Mac ARM64 
platforms and architectures.

The only prerequisite is to install Docker on your operating system.
You can find the information on [how to install it](https://docs.docker.com/get-docker/) 
depending on your distribution.

Once you have installed Docker, download the image from the image repository using this command:
* If you are using a Linux AMD64 distribution:
```console
> docker pull registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_linux:latest
```
* If you are using a Mac ARM64 distribution:
```console
> docker pull registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_mac_arm64v8:latest
```
Next, you will find the steps, depending on your distribution, to run Fall3D inside the container or using Docker volumes.

## Linux distribution
### Run image inside the container and run Fall3D example

* Run the image
```console
> docker run --rm --name fall3d -it registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_linux:latest
```
* Run the example in serial mode:
```console
> cd fall3d/Example
> ./Fall3d.r8.x All Example.inp
```
* Run the example in parallel mode:
 
Note: Adapt this command depending on your available resources. The 8 processors is the result from 4x2x1

```console
> cd fall3d/Example
> mpirun -n 8 --allow-run-as-root ./Fall3d.r8.x All Example.inp 4 2 1
```
### Run Fall3D with docker volumes

* Create the volume:
```console
> docker volume create fall3d_files
```
* Inside this new volume you have to include these files(you can obtain these files by clonning the Fall3D repository):
```console
ls /var/lib/docker/volumes/fall3d_files/_data
```
* Output:
```console
Example-8.0.wrf.nc  Example.inp  Example.pts  Example.tgsd
```
* Open the Example.inp file and insert the complete path of the Example-8.0.wrf.nc file:
```console
   METEO_DATA_FORMAT          = WRF
   METEO_DATA_DICTIONARY_FILE =
   METEO_DATA_FILE            = /home/fall3d/Example/Example-8.0.wrf.nc
   METEO_ENSEMBLE_BASEPATH    = ./
   METEO_LEVELS_FILE          = ../Other/Meteo/Tables/L137_ECMWF.levels
```
* Save and close the file

### Execute the container using the docker volume

* Serial:
```console
> docker run --rm -it --name fall3d-test -v fall3d_files:/home/fall3d/Example registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_linux:latest /home/fall3d/bin/Fall3d.r8.x All /home/fall3d/Example/Example.inp
```
* Parallel:
```console
> docker run --rm -it --name fall3d-test -v fall3d_files:/home/fall3d/Example registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_linux:latest mpirun -n 8 --allow-run-as-root /home/fall3d/bin/Fall3d.r8.x All /home/fall3d/Example/Example.inp 4 2 1
```

## MAC distribution
### Run image inside the container and run Fall3D example

* To run the image
```console
> docker run --rm --name fall3d -it registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_mac_arm64v8:latest sh
```
### Run fall3D Example
```console
> cd fall3d/Example
```
* Serial mode:
```console
> ./Fall3d.r8.x All Example.inp
```
* Parallel Mode:

Note: Adapt this command depending on your available resources.
The 8 processors is the result from 4x2x1
```console
> mpirun -n 8 --allow-run-as-root ./Fall3d.r8.x All Example.inp 4 2 1
```
## Run Fall3D with docker volumes

* Create the volume:
```console
> docker volume create --opt type=none --opt device=/Users/geo3bcn/Documents/DT-GEO/Dockers_MacOS --opt o=bind fall3d_files
```
1. `docker volume create`: This is the main command used to create a Docker volume.

2. `--opt type=none`: This option specifies that the volume type is "none", indicating that Docker will not manage the volume directly.

3. `--opt device=/Users/geo3bcn/Documents/DT-GEO/Dockers_MacOS`: This option specifies the device or path on the host system that will be used as the volume source. In this case, the directory `/Users/geo3bcn/Documents/DT-GEO/Dockers_MacOS` will be used.

4. `--opt o=bind`: This option specifies the mount options for the volume. In this case, it sets the mount option to "bind", meaning that the specified device or path will be directly mounted into the container.

5. `fall3d_files`: This is the name of the Docker volume that will be created. You can use this name to reference the volume when mounting it into containers.

In summary, this command creates a Docker volume named `fall3d_files` using the specified directory `/Users/geo3bcn/Documents/DT-GEO/Dockers_MacOS` on the host system. The volume is created with the "bind" type, meaning that the contents of the specified directory will be directly accessible within containers that use this volume.

Add the file shared in Docker Desktop

1. Open Docker Desktop in your MAC
2. Go to "Settings"
3. Now go to "Resources" and "File Sharing"
4. Add the specified directory used by the created volume, in this case `/Users/geo3bcn/Documents/DT-GEO/Dockers_MacOS`
5. Press "Apply & restart"

* Inside this new volume you have to include these files(you can obtain these files by clonning the Fall3D repository):
```console
> ls /Users/geo3bcn/Documents/DT-GEO/Dockers_MacOS/fall3d_files/_data
```
* Output:
```console
Example-8.0.wrf.nc  Example.inp  Example.pts  Example.tgsd
```
* Open Example.inp file and insert the complete path of the Example-8.0.wrf.nc file:
```console
   METEO_DATA_FORMAT          = WRF
   METEO_DATA_DICTIONARY_FILE =
   METEO_DATA_FILE            = /home/fall3d/Example/Example-8.0.wrf.nc
   METEO_ENSEMBLE_BASEPATH    = ./
   METEO_LEVELS_FILE          = ../Other/Meteo/Tables/L137_ECMWF.levels

```
* Save and close file

## Execute the container with the volume
* Serial:
```console
> docker run --rm -it --name fall3d-test -v fall3d_files:/home/fall3d/Example registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_mac_arm64v8:latest /home/fall3d/bin/Fall3d.r8.x All /home/fall3d/Example/Example.inp
```
* Parallel:
```console
> docker run --rm -it --name fall3d-test -v fall3d_files:/home/fall3d/Example registry.gitlab.com/fall3d-suite/fall3d/fall3d_alpine_mac_arm64v8:latest mpirun -n 8 --allow-run-as-root /home/fall3d/bin/Fall3d.r8.x All /home/fall3d/Example/Example.inp 4 2 1
```
